# themer

A small browser app for making color schemes.

This is my first Elm project; work is ongoing.

You can see it in action here:
[https://d2718.net/colors/](https://d2718.net/colors/)

You can click on the names of the colors ("bg 0", "fg 0", etc.) to edit
them into something more descriptive.
