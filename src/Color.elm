module Color exposing (Color, ReprBase, reprDeci, reprHexa, black, copy, cssStr, decoder, encode, hexStr, view, white)

{--
Color.elm

The central type of this module is the `Color`, which represents a 24-bit,
RGB-encoded color value.

This module also provides functions to represent colors hexadecimally and
as JSON, as well as render a color's adjustment section in the app view.
--}

import Html exposing (Attribute, Html, div, input)
import Html.Attributes exposing (attribute, class, title, type_, value)
import Html.Events exposing (onInput)
import Json.Decode as Decode
import Json.Encode as Encode



-- `bigA`, `smallA`, and `zero` are integer values that can be added to
-- a number (0-15) to yield the UCS of a character representation of the
-- corresponding hexadecimal digit.
--
-- Similarly, they can be _subtracted_ from the UCS of a hex digit to yield
-- its value as an integer.
-- For parsing upper-case hex digits or converting 10-15 into an upper-case
-- hex digit.


bigA : Int
bigA =
    Char.toCode 'A' - 10



-- For parsing lower-case hex digits or converting 10-15 into a lower-case
-- hex digit.


smallA : Int
smallA =
    Char.toCode 'a' - 10



-- For converting digits '0'-'9' and numbers 0-9.


zero : Int
zero =
    Char.toCode '0'


-- For switching between hex color entry and decimal color entry.

type ReprBase
    = Deci
    | Hexa

reprDeci : () -> ReprBase
reprDeci _ = Deci

reprHexa : () -> ReprBase
reprHexa _ = Hexa

-- Used in some functions to select which color element to update or extract.


type Element
    = R
    | G
    | B



-- The central type, representing a 24-bit RGB color value.


type alias Color =
    { r : Int
    , g : Int
    , b : Int
    }



-- Deep copy.


copy : Color -> Color
copy c =
    Color c.r c.g c.b



-- Update the given element of the Color.


update : Element -> Color -> Int -> Color
update elt c n =
    case elt of
        R ->
            { c | r = n }

        G ->
            { c | g = n }

        B ->
            { c | b = n }



-- Update the given element of the Color with the String output from an
-- HTML input.


updFromInput : ReprBase -> Element -> Color -> String -> Color
updFromInput base elt c val =
    let n = case base of
                Deci -> String.toInt val |> Maybe.withDefault 0
                Hexa -> parseHex val
    in modBy 256 n |> update elt c


-- Convert a number (mod 16) to a hexadecimal digit.


hexChar : Int -> Char
hexChar n =
    let
        val =
            modBy 16 n

        codepoint =
            if val < 10 then
                val + zero

            else
                val + smallA
    in
    Char.fromCode codepoint


-- Convert a hex character digit to its numeric value.

charHex : Char -> Int
charHex d =
    case d of
        '1' -> 1
        '2' -> 2
        '3' -> 3
        '4' -> 4
        '5' -> 5
        '6' -> 6
        '7' -> 7
        '8' -> 8
        '9' -> 9
        'a' -> 10
        'A' -> 10
        'b' -> 11
        'B' -> 11
        'c' -> 12
        'C' -> 12
        'd' -> 13
        'D' -> 13
        'e' -> 14
        'E' -> 14
        'f' -> 15
        'F' -> 15
        _ -> 0


-- Convert a number (mod 256) into a (hi, lo) pair of hex digits.


hexDigits : Int -> ( Char, Char )
hexDigits n =
    ( hexChar (modBy 256 n // 16)
    , hexChar (modBy 16 n)
    )

hexByte : Int -> String
hexByte n =
    let (hi, lo) = hexDigits n
    in String.fromList [ hi, lo ] 

-- Parse a string of hexadecimal digits into its numerical value.

parseHexAux : Int -> String -> Int
parseHexAux acc s =
    case String.uncons s of
        Nothing -> acc
        Just (ch, rest) -> parseHexAux ((charHex ch) + (16 * acc)) rest

parseHex : String -> Int
parseHex s = parseHexAux 0 s

-- Format a color as an `rrggbb` hexadecimal triplet.


hexStr : Color -> String
hexStr { r, g, b } =
    let
        ( rhi, rlo ) =
            hexDigits r

        ( ghi, glo ) =
            hexDigits g

        ( bhi, blo ) =
            hexDigits b
    in
    String.fromList [ rhi, rlo, ghi, glo, bhi, blo ]



-- Format a color as an `#rrggbb` hexadecimal triplet like you'd find
-- in a CSS property.


cssStr : Color -> String
cssStr c =
    hexStr c |> String.cons '#'


white : Color
white =
    Color 255 255 255


black : Color
black =
    Color 0 0 0


minAttr : Attribute a
minAttr =
    Html.Attributes.min "0"


maxAttr : Attribute a
maxAttr =
    Html.Attributes.max "255"



-- Render the Color adjustment section.

toBasedValue : ReprBase -> Int -> Html.Attribute a
toBasedValue base n =
    case base of
        Deci -> String.fromInt n |> value
        Hexa -> hexByte n |> value


view : (Color -> a) -> ReprBase -> Bool -> Color -> Html a
view updfn base expanded c =
    let
        rval = toBasedValue base c.r
        gval = toBasedValue base c.g
        bval = toBasedValue base c.b

        boxes =
            div [ class "color-inputs" ]
                [ input [ onInput (updFromInput base R c >> updfn), title "red", rval ] []
                , input [ onInput (updFromInput base G c >> updfn), title "green", gval ] []
                , input [ onInput (updFromInput base B c >> updfn), title "blue", bval ] []
                ]

        bars =
            if expanded then
                div [ class "color-bars" ]
                    [ input
                        [ type_ "range"
                        , String.fromInt c.r |> value
                        , minAttr
                        , maxAttr
                        , title "red"
                        , onInput (updFromInput Deci R c >> updfn)
                        ]
                        []
                    , Html.br [] []
                    , input
                        [ type_ "range"
                        , String.fromInt c.g |> value
                        , minAttr
                        , maxAttr
                        , title "green"
                        , onInput (updFromInput Deci G c >> updfn)
                        ]
                        []
                    , Html.br [] []
                    , input
                        [ type_ "range"
                        , String.fromInt c.b |> value
                        , minAttr
                        , maxAttr
                        , title "blue"
                        , onInput (updFromInput Deci B c >> updfn)
                        ]
                        []
                    ]

            else
                div [ class "empty" ] []
    in
    div [] [ boxes, bars ]



-- JSON-encode a color value.


encode : Color -> Encode.Value
encode c =
    Encode.object
        [ ( "r", Encode.int c.r )
        , ( "g", Encode.int c.g )
        , ( "b", Encode.int c.b )
        ]



-- A JSON-decoder for decoding a color value.


decoder : Decode.Decoder Color
decoder =
    Decode.map3 Color
        (Decode.field "r" Decode.int)
        (Decode.field "g" Decode.int)
        (Decode.field "b" Decode.int)
