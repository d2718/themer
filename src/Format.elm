module Format exposing (FormatType, colorView)

{-
Format.elm

For outputting palette information in different editor theme formats.

The idea with the output generated here is that one could copy and paste
it as the palette section of an editor (or other type of) theme file.
-}

import Color exposing (Color, hexStr)
import Html exposing (Html, div, input, label, text)
import Html.Attributes as Attributes exposing (class, type_)
import Html.Events as Events exposing (onClick)
import String.Interpolate exposing (interpolate)

import Style

-- Palette formats
type FormatType
    = Helix
    | LiteXl
    | Geany
    | GtkSourceView -- gedit, pluma, &c.

-- An abstraction over the two different types of palette elements 
-- (`Style.Bg` and `Style.Fg`) for accessing shared properties.
type NamedColor
    = Fore Style.Fg
    | Back Style.Bg

-- Return the name of the palette element.
colorName : NamedColor -> String
colorName nc =
    case nc of
        Fore fg ->
            Style.getFgName fg

        Back bg ->
            Style.getBgName bg

-- Get the `Color.Color` value of the palette element.
colorVal : NamedColor -> Color
colorVal nc =
    case nc of
        Fore fg ->
            fg.color

        Back bg ->
            bg.color
{-
When generating the copy-paste editor theme text, color names are filtered
to only keep letters, numbers, and underscores.

`okayChar` and `filterName` accomplish that.
-}

okayChar : Char -> Bool
okayChar c =
    Char.isAlphaNum c || c == '_'


filterName : String -> String
filterName name =
    String.filter okayChar name


-- Display a `Color`'s representation in the given theme format.
showColor : FormatType -> NamedColor -> String
showColor ft nc =
    let
        name =
            colorName nc |> filterName

        hex =
            colorVal nc |> hexStr
    in
    case ft of
        Helix ->
            interpolate "{0} = \"#{1}\"" [ name, hex ]

        LiteXl ->
            interpolate "local {0} = { common.color \"#{1}\" }" [ name, hex ]

        Geany ->
            interpolate "{0} = 0x{1}" [ name, hex ]

        GtkSourceView ->
            interpolate "<color name=\"{0}\" value=\"#{1}\"/>" [ name, hex ]

-- Combine a list of `Style.Bg`s and a list of `Style.Fg`s into a single
-- list of `NamedColor` s.
allColors : List NamedColor -> List Style.Bg -> List Style.Fg -> List NamedColor
allColors acc bgs fgs =
    case fgs of
        [] ->
            case bgs of
                [] ->
                    acc

                bg :: rest ->
                    allColors (Back bg :: acc) rest []

        fg :: rest ->
            allColors (Fore fg :: acc) bgs rest

-- Generate the copy-paste output text.
colorSection : FormatType -> List Style.Bg -> List Style.Fg -> String
colorSection ft bgs fgs =
    let
        colors =
            allColors [] bgs fgs |> List.map (showColor ft)
    in
    case ft of
        Helix ->
            String.join "\n" ("[palette]" :: colors)

        LiteXl ->
            String.join "\n" colors

        Geany ->
            String.join "\n" ("[named_colors]" :: colors)

        GtkSourceView ->
            String.join "\n" colors


-- Generate one of the radio buttons for selecting the output format.
radio : (Maybe FormatType -> a) -> Maybe FormatType -> String -> Html a
radio update mft labTxt =
    label []
        [ input
            [ type_ "radio"
            , Attributes.name "format-type-select"
            , onClick (update mft)
            ]
            []
        , text labTxt
        ]

-- Generate the "output" section of the app view.
colorView : (Maybe FormatType -> a) -> Maybe FormatType -> List Style.Bg -> List Style.Fg -> Html a
colorView update mft bgs fgs =
    let
        inputs =
            [ radio update Nothing "None"
            , radio update (Just Helix) "Helix"
            , radio update (Just LiteXl) "Lite XL"
            , radio update (Just Geany) "Geany"
            , radio update (Just GtkSourceView) "GtkSourceView"
            ]

        output =
            case mft of
                Nothing ->
                    text ""

                Just Helix ->
                    text (colorSection Helix bgs fgs)

                Just LiteXl ->
                    text (colorSection LiteXl bgs fgs)

                Just Geany ->
                    text (colorSection Geany bgs fgs)

                Just GtkSourceView ->
                    text (colorSection GtkSourceView bgs fgs)
    in
    div [ Attributes.id "format-section" ]
        [ div [ Attributes.id "output-format" ] inputs
        , div [ Attributes.id "output" ] [ output ]
        ]
