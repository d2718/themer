port module Main exposing (main)

import Browser
import Color exposing (Color)
import File exposing (File)
import File.Download as Download
import File.Select exposing (file)
import Format exposing (FormatType, colorView)
import Html exposing (Html, button, label, text, input)
import Html.Attributes as Attributes exposing (class, style, title, type_)
import Html.Events exposing (onClick, onInput)
import Model exposing (Model)
import Style
import Task


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , view = view
        }



-- Ports


port alert : String -> Cmd msg



-- The Model


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model.default (), Cmd.none )


loadFile : File -> Cmd Msg
loadFile f =
    Task.perform DecodeLoaded (File.toString f)



-- Messages


type Msg
    = NewBg
    | NewFg
    | UpdateBg Style.Bg
    | UpdateFg Style.Fg
    | DeleteBg Style.Bg
    | DeleteFg Style.Fg
    | ChangeBase Color.ReprBase
    | BodyColor String
    | Output (Maybe FormatType)
    | Save String
    | SelectFile
    | Load File
    | DecodeLoaded String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        BodyColor s ->
            ( Model.uBright model s, Cmd.none )

        NewBg ->
            ( Model.newBg model, Cmd.none )

        NewFg ->
            ( Model.newFg model, Cmd.none )

        UpdateBg bg ->
            ( Model.uBg model bg, Cmd.none )

        UpdateFg fg ->
            ( Model.uFg model fg, Cmd.none )

        DeleteBg bg ->
            ( Model.delBg model bg, Cmd.none )

        DeleteFg fg ->
            ( Model.delFg model fg, Cmd.none )

        Output mft ->
            ( Model.uOutput model mft, Cmd.none )

        ChangeBase b ->
            ( Model.uBase model b, Cmd.none )

        Save json ->
            let
                cmd =
                    Download.string "theme.json" "application/json" json
            in
            ( model, cmd )

        SelectFile ->
            ( model, file [ "application/json" ] Load )

        Load f ->
            ( model, loadFile f )

        DecodeLoaded json ->
            case Model.decode json of
                Ok loadedModel ->
                    ( loadedModel, Cmd.none )

                Err errorMessage ->
                    ( model, alert errorMessage )



-- View


view : Model -> Html Msg
view model =
    let
        bgStr =
            Color.Color model.brightness model.brightness model.brightness
                |> Color.cssStr

        fgStr =
            if model.brightness > 127 then
                "#444"

            else
                "#bbb"
    in
    Html.div
        [ style "background-color" bgStr
        , style "color" fgStr
        , Attributes.id "main"
        ]
        [ Html.h1 [] [ text "Color Scheme Designer" ]
        , Html.div [ Attributes.id "main-color" ]
            [ Html.input
                [ type_ "range"
                , Attributes.min "0"
                , Attributes.max "255"
                , title "background brightness"
                , onInput BodyColor
                ]
                []
            , Html.div [ Attributes.id "base-select" ]
                [ label [] [ input [ type_ "radio"
                                  , Attributes.name "base-select"
                                  , onClick (Color.reprDeci () |> ChangeBase)
                                  ] []
                           , text "Dec"
                           ]
                , label [] [ input [ type_ "radio"
                                   , Attributes.name "base-select"
                                   , onClick (Color.reprHexa () |> ChangeBase)
                                   ] []
                           , text "Hex"
                           ]
                ]
            ]
        , Html.div [ Attributes.id "tables" ]
            [ Html.table []
                [ Html.tbody []
                    (model.backgrounds
                        |> List.reverse
                        |> List.map (Style.viewBg UpdateBg DeleteBg model.base)
                    )
                ]
            , Html.table []
                [ Html.tbody []
                    (model.foregrounds
                        |> List.reverse
                        |> List.map (Style.viewFg UpdateFg DeleteFg model.base model.backgrounds)
                    )
                ]
            ]
        , Html.div [ Attributes.id "add-buttons" ]
            [ button [ onClick NewBg ] [ text "[ + background color ]" ]
            , button [ onClick NewFg ] [ text "[ + foreground color ]" ]
            ]
        , colorView Output model.output model.backgrounds model.foregrounds
        , Html.div [ Attributes.id "persist" ]
            [ button [ onClick (Model.jsonify model |> Save) ] [ text "▼ save" ] -- 25bc \u{027f1}
            , button [ onClick SelectFile ] [ text "load ▲" ] -- 25b2 \u{027f0}
            ]
        ]
