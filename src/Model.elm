module Model exposing
    ( Model
    , decode
    , decoder
    , default
    , delBg
    , delFg
    , encode
    , jsonify
    , newBg
    , newFg
    , uBg
    , uBright
    , uFg
    , uBase
    , uOutput
    )


import Json.Decode as Decode
import Json.Encode as Encode

import Color exposing (Color, ReprBase)
import Format exposing (FormatType)
import Style


type alias Model =
    { curId : Int
    , brightness : Int
    , backgrounds : List Style.Bg
    , foregrounds : List Style.Fg
    , output : Maybe FormatType
    , base : ReprBase
    }


default : () -> Model
default _ =
    { curId = 1
    , brightness = 64
    , backgrounds = [ Style.defaultBg 0 ]
    , foregrounds = [ Style.defaultFg 1 ]
    , output = Nothing
    , base = Color.reprDeci ()
    }


uBright : Model -> String -> Model
uBright model val =
    let
        n =
            String.toInt val
                |> Maybe.withDefault 0
                |> modBy 256
    in
    { model | brightness = n }


uOutput : Model -> Maybe FormatType -> Model
uOutput model mft =
    { model | output = mft }


uBg : Model -> Style.Bg -> Model
uBg model bg =
    { model
        | backgrounds =
            model.backgrounds
                |> List.map
                    (\x ->
                        if x.id == bg.id then
                            bg

                        else
                            x
                    )
    }


uFg : Model -> Style.Fg -> Model
uFg model fg =
    { model
        | foregrounds =
            model.foregrounds
                |> List.map
                    (\x ->
                        if x.id == fg.id then
                            fg

                        else
                            x
                    )
    }


delBg : Model -> Style.Bg -> Model
delBg model bg =
    { model
        | backgrounds =
            model.backgrounds
                |> List.filter (\x -> x.id /= bg.id)
    }


delFg : Model -> Style.Fg -> Model
delFg model fg =
    { model
        | foregrounds =
            model.foregrounds
                |> List.filter (\x -> x.id /= fg.id)
    }


newBg : Model -> Model
newBg model =
    let
        nextId =
            model.curId + 1

        nbg =
            case List.head model.backgrounds of
                Just bg ->
                    Style.deriveBg nextId bg

                Nothing ->
                    Style.defaultBg nextId
    in
    { model
        | curId = nextId
        , backgrounds = nbg :: model.backgrounds
    }


newFg : Model -> Model
newFg model =
    let
        nextId =
            model.curId + 1

        nfg =
            case List.head model.foregrounds of
                Just fg ->
                    Style.deriveFg nextId fg

                Nothing ->
                    Style.defaultFg nextId
    in
    { model
        | curId = nextId
        , foregrounds = nfg :: model.foregrounds
    }


uBase : Model -> ReprBase -> Model
uBase model base = { model | base = base }

-- Encoding/Decoding


encode : Model -> Encode.Value
encode model =
    Encode.object
        [ ( "curId", Encode.int model.curId )
        , ( "brightness", Encode.int model.brightness )
        , ( "backgrounds", Encode.list Style.encodeBg model.backgrounds )
        , ( "foregrounds", Encode.list Style.encodeFg model.foregrounds )
        ]


decodeHelper : Int -> Int -> List Style.Bg -> List Style.Fg -> Model
decodeHelper id brt bgs fgs =
    Model id brt bgs fgs Nothing (Color.reprDeci ())


decoder : Decode.Decoder Model
decoder =
    Decode.map4 decodeHelper
        (Decode.field "curId" Decode.int)
        (Decode.field "brightness" Decode.int)
        (Decode.field "backgrounds" (Decode.list Style.decoderBg))
        (Decode.field "foregrounds" (Decode.list Style.decoderFg))


jsonify : Model -> String
jsonify model =
    encode model |> Encode.encode 0


decode : String -> Result String Model
decode json =
    case Decode.decodeString decoder json of
        Ok model ->
            Ok model

        Err e ->
            Err (Decode.errorToString e)
