module Style exposing
    ( Bg
    , Fg
    , decoderBg
    , decoderFg
    , defaultBg
    , defaultFg
    , deriveBg
    , deriveFg
    , encodeBg
    , encodeFg
    , getBgName
    , getFgName
    , viewBg
    , viewFg
    )

import Color exposing (Color, ReprBase)
import Html exposing (Html, button, text)
import Html.Attributes as Attributes exposing (class, style, title, type_, value)
import Html.Events as Events exposing (onClick, onInput)
import Json.Decode as Decode
import Json.Encode as Encode


type Text
    = Display String
    | Editing String


getText : Text -> String
getText t =
    case t of
        Display s ->
            s

        Editing s ->
            s


type alias Bg =
    { id : Int
    , text : Text
    , color : Color
    , expanded : Bool
    }


getBgName : Bg -> String
getBgName bg =
    getText bg.text


defaultBg : Int -> Bg
defaultBg n =
    { id = n
    , text = Display ("bg " ++ String.fromInt n)
    , color = Color.black
    , expanded = False
    }


deriveBg : Int -> Bg -> Bg
deriveBg n bg =
    { id = n
    , text = Display ("bg " ++ String.fromInt n)
    , color = Color.copy bg.color
    , expanded = False
    }


type alias Fg =
    { id : Int
    , text : Text
    , color : Color
    , bg : Int
    , expanded : Bool
    }


getFgName : Fg -> String
getFgName fg =
    getText fg.text


defaultFg : Int -> Fg
defaultFg n =
    { id = n
    , text = Display ("fg " ++ String.fromInt n)
    , color = Color.white
    , bg = 0
    , expanded = False
    }


deriveFg : Int -> Fg -> Fg
deriveFg n fg =
    { id = n
    , text = Display ("fg " ++ String.fromInt n)
    , color = Color.copy fg.color
    , bg = fg.bg
    , expanded = False
    }


contrast : Color -> Color
contrast c =
    let
        eltSums =
            toFloat c.r + toFloat c.g + toFloat c.b

        avg =
            eltSums / 3.0
    in
    if avg > 127.5 then
        Color.black

    else
        Color.white



-- VIEW (Bg)


updateBgText : Bg -> String -> Bg
updateBgText bg s =
    { bg | text = Display s }


labelBg : (Bg -> a) -> Bg -> Html a
labelBg update bg =
    case bg.text of
        Display s ->
            Html.span [ onClick ({ bg | text = Editing s } |> update) ]
                [ text s ]

        Editing s ->
            let
                decoder : Decode.Decoder a
                decoder =
                    Decode.map (updateBgText bg >> update)
                        (Decode.at [ "target", "value" ] Decode.string)
            in
            Html.input [ Events.on "change" decoder, value s ] []


expandBg : (Bg -> a) -> Bg -> Html a
expandBg update bg =
    if bg.expanded then
        button
            [ title "hide color sliders"
            , onClick ({ bg | expanded = False } |> update)
            ]
            [ text "[ ^ ]" ]

    else
        button
            [ title "show color sliders"
            , onClick ({ bg | expanded = True } |> update)
            ]
            [ text "[ v ]" ]


viewBg : (Bg -> a) -> (Bg -> a) -> ReprBase -> Bg -> Html a
viewBg update delete base bg =
    let
        bgStr =
            Color.cssStr bg.color

        fgStr =
            contrast bg.color |> Color.cssStr

        updfn : Color -> a
        updfn c =
            { bg | color = c } |> update
    in
    Html.tr
        [ style "color" fgStr, style "background-color" bgStr ]
        [ Html.td [ class "label" ] [ labelBg update bg ]
        , Html.td [] [ text bgStr ]
        , Html.td [] [ expandBg update bg ]
        , Html.td [ class "inputs" ] [ Color.view updfn base bg.expanded bg.color ]
        , Html.td [] [ button [ onClick (delete bg) ] [ text "[ - ]" ] ]
        ]



-- VIEW (Fg)


updateFgText : Fg -> String -> Fg
updateFgText fg s =
    { fg | text = Display s }


updateFgBg : Fg -> String -> Fg
updateFgBg fg val =
    { fg | bg = String.toInt val |> Maybe.withDefault 0 }


labelFg : (Fg -> a) -> Fg -> Html a
labelFg update fg =
    case fg.text of
        Display s ->
            Html.span [ onClick ({ fg | text = Editing s } |> update) ]
                [ text s ]

        Editing s ->
            let
                decoder : Decode.Decoder a
                decoder =
                    Decode.map (updateFgText fg >> update)
                        (Decode.at [ "target", "value" ] Decode.string)
            in
            Html.input [ Events.on "change" decoder, value s ] []


expandFg : (Fg -> a) -> Fg -> Html a
expandFg update fg =
    if fg.expanded then
        button [ onClick ({ fg | expanded = False } |> update) ] [ text "[ ^ ]" ]

    else
        button [ onClick ({ fg | expanded = True } |> update) ] [ text "[ v ]" ]


bgToOption : Bg -> Html a
bgToOption bg =
    let
        label =
            case bg.text of
                Display s ->
                    s

                Editing s ->
                    s
    in
    Html.option [ String.fromInt bg.id |> value ] [ text label ]


bgChooseFg : (Fg -> a) -> List Bg -> Fg -> Html a
bgChooseFg update bgs fg =
    let
        decoder : Decode.Decoder a
        decoder =
            Decode.map (updateFgBg fg >> update)
                (Decode.at [ "target", "value" ] Decode.string)
    in
    Html.select [ Events.on "input" decoder, String.fromInt fg.bg |> value ]
        (List.map bgToOption bgs |> List.reverse)


viewFg : (Fg -> a) -> (Fg -> a) -> Color.ReprBase -> List Bg -> Fg -> Html a
viewFg update delete base bgs fg =
    let
        bgStr =
            List.filterMap
                (\bg ->
                    if bg.id == fg.bg then
                        Just bg.color

                    else
                        Nothing
                )
                bgs
                |> List.head
                |> Maybe.withDefault Color.black
                |> Color.cssStr

        fgStr =
            Color.cssStr fg.color

        updfn : Color -> a
        updfn c =
            { fg | color = c } |> update
    in
    Html.tr
        [ style "color" fgStr, style "background-color" bgStr ]
        [ Html.td [ class "label" ] [ labelFg update fg ]
        , Html.td [] [ String.join " " [ fgStr, "[", bgStr, "]" ] |> text ]
        , Html.td [] [ expandFg update fg ]
        , Html.td [ class "inputs" ] [ Color.view updfn base fg.expanded fg.color ]
        , Html.td [ class "select" ] [ bgChooseFg update bgs fg ]
        , Html.td [] [ button [ onClick (delete fg) ] [ text "[ - ]" ] ]
        ]



-- Encoding/Decoding


encodeBg : Bg -> Encode.Value
encodeBg bg =
    Encode.object
        [ ( "id", Encode.int bg.id )
        , ( "text", Encode.string (getText bg.text) )
        , ( "color", Color.encode bg.color )
        ]


decodeBgHelper : Int -> String -> Color -> Bg
decodeBgHelper n name c =
    Bg n (Display name) c False


decoderBg : Decode.Decoder Bg
decoderBg =
    Decode.map3 decodeBgHelper
        (Decode.field "id" Decode.int)
        (Decode.field "text" Decode.string)
        (Decode.field "color" Color.decoder)


encodeFg : Fg -> Encode.Value
encodeFg fg =
    Encode.object
        [ ( "id", Encode.int fg.id )
        , ( "text", Encode.string (getText fg.text) )
        , ( "color", Color.encode fg.color )
        , ( "bg", Encode.int fg.bg )
        ]


decodeFgHelper : Int -> String -> Color -> Int -> Fg
decodeFgHelper n name c m =
    Fg n (Display name) c m False


decoderFg : Decode.Decoder Fg
decoderFg =
    Decode.map4 decodeFgHelper
        (Decode.field "id" Decode.int)
        (Decode.field "text" Decode.string)
        (Decode.field "color" Color.decoder)
        (Decode.field "bg" Decode.int)
